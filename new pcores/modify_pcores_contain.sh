#!/bin/sh
cd test_v1_0_0/
#rename tcl file
for old_name in nic_output_port_lookup*
do
echo $old_name | sed s/nic_output_port_lookup/test/p > tmp.log
new_name=`tail -n 1 tmp.log`
mv $old_name $new_name
rm tmp.log
done
#modify content of tcl file
sed -i 's/nic_output_port_lookup/test/g' *.tcl
sed -i 's/output_port_lookup/test/g' *.tcl
#modify Makefile
sed -i 's/nic_output_port_lookup/test/g' Makefile
sed -i 's/output_port_lookup/test/g' Makefile
cd data/
#rename files
for old_name in output_port_lookup*
do
echo $old_name | sed s/output_port_lookup/test/p > tmp.log
new_name=`tail -n 1 tmp.log`
mv $old_name $new_name
rm tmp.log
done
sed -i 's/OUTPUT_PORT_LOOKUP/test/g' *.h
sed -i 's/OUTPUT_PORT_LOOKUP/test/g' *.tcl
sed -i 's/OUTPUT_PORT_LOOKUP/test/g' *.txt
cd ../hdl/
#rename files
for old_name in output_port_lookup*
do
echo $old_name | sed s/output_port_lookup/test/p > tmp.log
new_name=`tail -n 1 tmp.log`
mv $old_name $new_name
rm tmp.log
done
for old_name in nic_output_port_lookup*
do
echo $old_name | sed s/nic_output_port_lookup/test/p > tmp.log
new_name=`tail -n 1 tmp.log`
mv $old_name $new_name
rm tmp.log
done
sed -i 's/output_port_lookup/test/g' *.v
sed -i 's/nic_output_port_lookup/test/g' *.v
