#!/bin/sh
cd hw/
#modify content of Makefile
sed -i 's/reference_nic/test/g' Makefile
cd constraints/
#modify content of xdc file
sed -i 's/reference_nic/test/g' *.xdc
cd ../hdl/
#modify content of verilog file
sed -i 's/reference_nic/test/g' *.v
cd ../tcl/
#rename tcl file
for old_name in reference_nic*
do
echo $old_name | sed s/reference_nic/test/p > tmp.log
new_name=`tail -n 1 tmp.log`
mv $old_name $new_name
rm tmp.log
done
#modify content of tcl file
sed -i 's/reference_nic/test/g' *.tcl
cd ../../sw/embedded/
#modify content of Makefile
sed -i 's/reference_nic/test/g' Makefile
cd src/
#modify content of C files
sed -i 's/reference_nic/test/g' *.c
#modify content of heaader files
sed -i 's/reference_nic/test/g' *.h
cd ../xml/
#modify content of xml files
sed -i 's/reference_nic/test/g' *.xml
cd ../tcl
for old_name in reference_nic*
do
echo $old_name | sed s/reference_nic/test/p > tmp.log
new_name=`tail -n 1 tmp.log`
mv $old_name $new_name
rm tmp.log
done
#modify content of tcl files
sed -i 's/reference_nic/test/g' *.tcl

